﻿using System;
using System.Linq;

namespace TextToBaseEncoding
{
    internal class Program
    {
        private static readonly char[] Base64Letters =
        {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
            'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
        };

        private static void Main()
        {
            Console.Out.Write("Enter a text: ");
            var text = Console.In.ReadLine();

            if (string.IsNullOrEmpty(text))
            {
                Console.Error.WriteLine("Invalid input!");
                return;
            }

            var ord = text.Select(Convert.ToByte).ToArray();

            var ordDecStr = ord.Select(b => Convert.ToString(b));
            Console.Out.WriteLine("Decimal:");
            Console.Out.WriteLine(string.Join(' ', ordDecStr));

            var ordBinStr = ord.Select(b => Convert.ToString(b, 2).PadLeft(8, '0')).ToArray();
            Console.Out.WriteLine("8 Binary:");
            Console.Out.WriteLine(string.Join(' ', ordBinStr));

            var rawBinary = string.Concat(ordBinStr);
            Console.Out.WriteLine("Convert to a single binary:");
            Console.Out.WriteLine(rawBinary);


            var base64 = ConvertToBase64(rawBinary);
            Console.Out.WriteLine();
            Console.Out.WriteLine("Base64: {0}", base64);
        }

        private static string ConvertToBase64(string input)
        {
            const byte sixBits = 6;
            const byte chunk = 24;
            var text = new string(input);
            var output = string.Empty;

            if (input.Length % sixBits != 0)
            {
                var pad = (input.Length / sixBits + 1) * sixBits;
                Console.Out.WriteLine("Length: {0} - Pad: {1}", input.Length, pad);
                text = input.PadRight(pad, '0');
            }

            Console.Out.WriteLine("Binary with padding right:");
            Console.Out.WriteLine(text);

            Console.Out.WriteLine("6 Binary");
            for (int i = 0; i < text.Length; i += 6)
            {
                var sixByte = text.Substring(i, sixBits);
                Console.Out.Write($"{sixByte} ");
                var sixInt = Convert.ToInt32(sixByte, 2);
                output += Base64Letters[sixInt].ToString();
            }

            var padding = (chunk - text.Length % chunk) / sixBits;
            output += new string('=', padding);

            return output;
        }
    }
}